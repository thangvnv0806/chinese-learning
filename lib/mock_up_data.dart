import 'model/comment.dart';

const List<Map> data = [
  {
    'url':
        'https://github.com/GeekyAnts/flick-video-player-demo-videos/blob/master/example/the_valley_compressed.mp4?raw=true',
    'thumbnail': "https://i.ytimg.com/vi/3R6KnQLvZNI/maxresdefault.jpg",
    'title': 'Complete flutter course with 14+ apps',
    'date': 'May 15, 2019',
    'creator': 'Hitesh Choudhary',
    'profile_url':
        'https://yt3.ggpht.com/a/AGF-l7-GpYFwHDMQVXkOcO3Ra8bIoZhhiU3oluiJBw=s88-mo-c-c0xffffffff-rj-k-no',
  },
  {
    'url':
        'https://github.com/GeekyAnts/flick-video-player-demo-videos/blob/master/example/the_valley_compressed.mp4?raw=true',
    'thumbnail': 'https://i.ytimg.com/vi/sPW7nDBqt8w/maxresdefault.jpg',
    'title': 'The Flutter YouTube Channel is Here!',
    'date': 'Feb 22, 2019',
    'creator': 'Flutter',
    'profile_url':
        'https://yt3.ggpht.com/a/AGF-l7-pLWHhqjLR5ZVoKzV9_eU6IjYrDyhvSLRjsw=s88-mo-c-c0xffffffff-rj-k-no',
  },
  {
    'url': 'https://www.youtube.com/watch?v=vqPG1tU6-c0',
    'thumbnail': 'https://i.ytimg.com/vi/vqPG1tU6-c0/maxresdefault.jpg',
    'title': 'Introducing The Boring Show!',
    'date': 'Feb 22, 2019',
    'creator': 'Flutter',
    'profile_url':
        'https://yt3.ggpht.com/a/AGF-l7-pLWHhqjLR5ZVoKzV9_eU6IjYrDyhvSLRjsw=s88-mo-c-c0xffffffff-rj-k-no',
  },
  {
    'url': 'https://www.youtube.com/watch?v=frEG8f0Aa1c',
    'thumbnail': 'https://i.ytimg.com/vi/frEG8f0Aa1c/maxresdefault.jpg',
    'title': 'Flutter vs React native',
    'date': 'Apr 10, 2019',
    'creator': 'Hitesh Choudhary',
    'profile_url':
        'https://yt3.ggpht.com/a/AGF-l7-GpYFwHDMQVXkOcO3Ra8bIoZhhiU3oluiJBw=s88-mo-c-c0xffffffff-rj-k-no',
  },
  {
    'url': 'https://youtu.be/GE0oeBj9Cr0',
    'thumbnail': 'https://i.ytimg.com/vi/GE0oeBj9Cr0/maxresdefault.jpg',
    'title': 'How to create first flutter for web project   step by step',
    'date': 'May 11, 2019',
    'creator': 'Hitesh Choudhary',
    'profile_url':
        'https://yt3.ggpht.com/a/AGF-l7-GpYFwHDMQVXkOcO3Ra8bIoZhhiU3oluiJBw=s88-mo-c-c0xffffffff-rj-k-no',
  },
];

List<String> favorites = [
  'https://www.youtube.com/watch?v=3R6KnQLvZNI',
  'https://www.youtube.com/watch?v=vqPG1tU6-c0'
];

List<Comment> comments = [
  Comment(content: 'abc', dateTime: DateTime.now(), name: 'User'),
  Comment(content: 'abc', dateTime: DateTime.now(), name: 'User'),
  Comment(content: 'abc', dateTime: DateTime.now(), name: 'User'),
  Comment(content: 'abc', dateTime: DateTime.now(), name: 'User'),
];
