import 'package:chinese_learning/mock_up_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'video_player_page.dart';

class VideoFeed extends StatefulWidget {
  VideoFeed({Key key}) : super(key: key);

  _VideoFeedState createState() => _VideoFeedState();
}

class _VideoFeedState extends State<VideoFeed> {
  @override
  Widget build(BuildContext context) {
    return AnimationLimiter(
      child: GridView.count(
        crossAxisCount: 2,
        children: data
            .asMap()
            .map((index, data) => MapEntry(
                index,
                AnimationConfiguration.staggeredGrid(
                  columnCount: 2,
                  position: index,
                  child: ScaleAnimation(
                    child: FadeInAnimation(
                      child: VideoThumbnail(
                        data: data,
                        favorite: favorites.contains(data['url']),
                      ),
                    ),
                  ),
                )))
            .values
            .toList(),
        childAspectRatio: 0.8,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        shrinkWrap: true,
      ),
    );
  }
}

class VideoThumbnail extends StatelessWidget {
  const VideoThumbnail({Key key, @required this.data, this.favorite = false})
      : super(key: key);

  final Map data;
  final bool favorite;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => VideoPlayerPage(
          data: data,
        ),
      )),
      splashColor: Theme.of(context).primaryColor,
      focusColor: Theme.of(context).primaryColor,
      child: Column(
        children: <Widget>[
          AspectRatio(
            child: Image(
              image: NetworkImage(data['thumbnail']),
              centerSlice: Rect.largest,
            ),
            aspectRatio: 16 / 9,
          ),
          ListTile(
              title: Text(
            data['title'],
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(data['profile_url']),
            ),
            title: Text(
              data['creator'] + " . " + data['date'],
              style: TextStyle(
                color: Colors.grey,
              ),
              overflow: TextOverflow.ellipsis,
            ),
            trailing: Icon(Icons.favorite,
                color: favorite
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).iconTheme.color),
          ),
        ],
      ),
    );
  }
}
