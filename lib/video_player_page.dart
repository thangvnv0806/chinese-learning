import 'package:chinese_learning/mock_up_data.dart';
import 'package:chinese_learning/video/data_manager.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'video/custom_orientation_control.dart';

class VideoPlayerPage extends StatefulWidget {
  VideoPlayerPage({Key key, @required this.data}) : super(key: key);

  final Map data;
  get url => data['url'];

  @override
  _VideoPlayerPageState createState() => _VideoPlayerPageState();
}

class _VideoPlayerPageState extends State<VideoPlayerPage> {
  FlickManager flickManager;
  DataManager dataManager;
  final urls = data.map<String>((map) => map['url']).toList();

  @override
  void initState() {
    super.initState();
    flickManager = FlickManager(
        videoPlayerController: VideoPlayerController.network(widget.url),
        onVideoEnd: () {
          dataManager.skipToNextVideo(Duration(seconds: 5));
        });

    dataManager = DataManager(
        flickManager: flickManager,
        urls: urls,
        currentPlaying: urls.indexOf(widget.url));
  }

  @override
  void dispose() {
    flickManager.dispose();
    super.dispose();
  }

  skipToVideo(String url) {
    flickManager.handleChangeVideo(VideoPlayerController.network(url));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: VisibilityDetector(
        key: ObjectKey(flickManager),
        onVisibilityChanged: (visibility) {
          if (visibility.visibleFraction == 0 && this.mounted) {
            flickManager.flickControlManager.autoPause();
          } else if (visibility.visibleFraction == 1) {
            flickManager.flickControlManager.autoResume();
          }
        },
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              child: FlickVideoPlayer(
                flickManager: flickManager,
                preferredDeviceOrientationFullscreen: [
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.landscapeLeft,
                  DeviceOrientation.landscapeRight,
                ],
                flickVideoWithControls: FlickVideoWithControls(
                  controls: CustomOrientationControls(dataManager: dataManager),
                ),
                flickVideoWithControlsFullscreen: FlickVideoWithControls(
                  videoFit: BoxFit.fitWidth,
                  controls: CustomOrientationControls(dataManager: dataManager),
                ),
              ),
            ),
            ListTile(
                title: Text(
              widget.data['title'],
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold),
            )),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(widget.data['profile_url']),
              ),
              title: Text(
                widget.data['creator'] + " . " + widget.data['date'],
                style: TextStyle(
                  color: Colors.grey,
                ),
                overflow: TextOverflow.ellipsis,
              ),
              trailing: Icon(Icons.favorite,
                  color: favorites.contains(widget.url)
                      ? Theme.of(context).primaryColor
                      : Theme.of(context).iconTheme.color),
            ),
            AnimationLimiter(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: comments.length,
                itemBuilder: (BuildContext context, int index) {
                  return AnimationConfiguration.staggeredList(
                    position: index,
                    duration: const Duration(milliseconds: 375),
                    child: SlideAnimation(
                      verticalOffset: 100.0,
                      child: FadeInAnimation(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Container(
                            color: Colors.grey[200],
                            child: ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                    widget.data['profile_url'],
                                  ),
                                ),
                                title: Text(
                                  comments[index].name,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text(comments[index].content),
                                trailing: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.thumb_up),
                                        onPressed: () {}),
                                    IconButton(
                                        icon: Icon(Icons.thumb_down),
                                        onPressed: () {}),
                                  ],
                                )),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
