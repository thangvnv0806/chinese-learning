import 'package:chinese_learning/question_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class ReviewPage extends StatelessWidget {
  final List<Review> reviews = [
    Review(name: 'TUYỆT VỜI', isSpecial: true),
    Review(name: 'Thanh mẫu\n87%'),
    Review(name: 'Vận mẫu\n87%'),
    Review(name: 'Thanh điệu\n87%'),
    Review(name: 'Ngữ điệu\n87%'),
  ];

  @override
  Widget build(BuildContext context) {
    final normalTextStyle =
        Theme.of(context).textTheme.headline6.apply(color: Colors.black);
    final specialTextStyle = Theme.of(context)
        .textTheme
        .headline4
        .apply(color: Theme.of(context).primaryColor);
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Image.asset(
            'images/logo.png',
            height: 50,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
        body: Padding(
          padding: const EdgeInsets.all(32),
          child: AnimationLimiter(
            child: ListView.builder(
              itemCount: reviews.length,
              itemBuilder: (BuildContext context, int index) {
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 100.0,
                    child: FadeInAnimation(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 32),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => QuestionPage())),
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text(
                            reviews[index].name,
                            style: reviews[index].isSpecial
                                ? specialTextStyle
                                : normalTextStyle,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }
}

class Review {
  String name;
  bool isSpecial;
  Review({@required this.name, this.isSpecial = false});
}
